// Package rzhttp provides an helper middleware to log HTTP requests
// See https://gitlab.com/bloom42/libs/rz-go/tree/master/examples/http for a working example
package rzhttp
